import pandas as pd
import duckdb
import os
import requests
import argparse

# Functie om alle CSV-bestanden in een map te laden
def load_csv_files(directory, pattern=None):
    dataframes = []
    for filename in os.listdir(directory):
        if filename.endswith(".csv") and (pattern is None or pattern in filename):
            print(f"reading file {filename}")
            df = pd.read_csv(os.path.join(directory, filename))
            df['source_file'] = filename  # Voeg een kolom toe om de bron te identificeren
            dataframes.append(df)
    return pd.concat(dataframes, ignore_index=True) if dataframes else pd.DataFrame()

# Functie om alle TXT-bestanden in een map te laden
def load_txt_files(directory, pattern=None):
    dataframes = []
    for filename in os.listdir(directory):
        if filename.endswith(".txt") and (pattern is None or pattern in filename):
            print(f"reading file {filename}")
            df = pd.read_csv(os.path.join(directory, filename), sep='\t', header=None)
            df.columns = ['timestamp', 'message']
            df['source_file'] = filename  # Voeg een kolom toe om de bron te identificeren
            dataframes.append(df)
    return pd.concat(dataframes, ignore_index=True) if dataframes else pd.DataFrame()

# Functie om API gegevens te laden en sub-endpoints te benaderen
def fetch_api_data(base_url, endpoint):
    try:
        if not base_url.startswith('http://') and not base_url.startswith('https://'):
            base_url = 'http://' + base_url
        response = requests.get(f"{base_url}/{endpoint}")
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"Error fetching data from {base_url}/{endpoint}: {e}")
        return pd.DataFrame()
    
    ids = response.json()
    if not isinstance(ids, list):
        return pd.DataFrame()

    # Fetch detailed data for each item
    detailed_data = []
    for item_id in ids:
        try:
            print(f"fetching {item_id}")
            detail_response = requests.get(f"{base_url}/{endpoint}/{item_id}")
            detail_response.raise_for_status()
            detailed_data.append(detail_response.json())
        except requests.exceptions.RequestException as e:
            print(f"Error fetching detail for {item_id}: {e}")
    
    return pd.DataFrame(detailed_data)

# Functie om ongeldige tekens in kolomnamen te vervangen
def clean_column_names(df):
    df.columns = [col.replace(":", "_").replace(" ", "_").replace(".", "_").replace("-", "_") for col in df.columns]
    return df

# Functie om te controleren of een tabel bestaat
def table_exists(con, table_name):
    return con.execute(f"SELECT COUNT(*) FROM information_schema.tables WHERE table_name = '{table_name}'").fetchone()[0] > 0

def fix_weather_timestamps(df):
    df['Time'] = pd.to_timedelta(df['Time']) + pd.Timestamp('1900-01-01')
    df = df.drop_duplicates(subset='Time')
    return df

# Functie om unieke data incrementieel te laden
def load_incremental_dataframe(con, df, table_name, unique_key):
    df = clean_column_names(df)  # Kolomnamen schoonmaken

    # Voor samengestelde sleutel
    if isinstance(unique_key, list):
        df['composite_key'] = df[unique_key].astype(str).agg('-'.join, axis=1)
        unique_key = 'composite_key'

    # Dubbele rijen verwijderen
    df = df.drop_duplicates(subset=unique_key)
    
    if not table_exists(con, table_name):
        # Maak de tabel als deze nog niet bestaat
        columns = ', '.join([f"{col} VARCHAR" for col in df.columns])
        create_table_query = f"CREATE TABLE {table_name} ({columns})"
        con.execute(create_table_query)
    
    # Haal de bestaande unieke keys op
    if table_exists(con, table_name):
        existing_keys = set(con.execute(f"SELECT {unique_key} FROM {table_name}").fetchdf()[unique_key].tolist())
    else:
        existing_keys = set()
    
    # Filter de nieuwe data om alleen unieke records toe te voegen
    new_data = df[~df[unique_key].isin(existing_keys)]
    print(f"Aantal nieuwe enrtrys in {table_name}: {len(new_data)}")
    
    # Voeg nieuwe data toe aan de tabel
    if not new_data.empty:
        con.execute(f"INSERT INTO {table_name} SELECT * FROM new_data")

# Functie om gegevens uit de directory en API te laden
def load_data(directory, base_url=None):
    specific_csv_files = {
        'circuits': 'circuits.csv',
        'constructor_results': 'constructor_results.csv',
        'driver_results': 'driver_results.csv',
        'driver_sprint_results': 'driver_sprint_results.csv',
        'drivers_teams': 'drivers_teams.csv',
        'lap_times': 'lap_times.csv'
    }

    specific_dataframes = {}
    for table_name, filename in specific_csv_files.items():
        filepath = os.path.join(directory, filename)
        if os.path.exists(filepath):
            df = pd.read_csv(filepath)
            specific_dataframes[table_name] = df

    laps_df = load_csv_files(directory, 'laps')
    weather_df = load_csv_files(directory, 'weather')
    race_control_df = load_txt_files(directory, 'racecontrol')

    if not weather_df.empty:
        weather_df = fix_weather_timestamps(weather_df)

    drivers_df, teams_df = pd.DataFrame(), pd.DataFrame()
    if base_url:
        drivers_df = fetch_api_data(base_url, 'drivers')
        teams_df = fetch_api_data(base_url, 'teams')

    return specific_dataframes, laps_df, weather_df, race_control_df, drivers_df, teams_df

def main(data_directory, base_url):
    con = duckdb.connect('f1_database.duckdb')

    unique_keys = {
        'circuits': ['latitude', 'longitude'],
        'constructor_results': ['year','name','team'],
        'driver_results': ['year','name','carNumber'],
        'driver_sprint_results': ['year','name','carNumber'],
        'drivers_teams': ['year','race_nr','driver','name'],
        'lap_times': ['date','carNumber','lap'],
        'raw_laps': ['DriverNumber','LapNumber','source_file'],
        'raw_weather': ['Time','source_file'],
        'raw_race_control': ['timestamp','message','source_file'],
        'raw_drivers': 'id',
        'raw_teams': 'id'
    }

    specific_dataframes, laps_df, weather_df, race_control_df, drivers_df, teams_df = load_data(data_directory, base_url)

    for table_name, df in specific_dataframes.items():
        raw_table_name = f'raw_{table_name}'
        load_incremental_dataframe(con, df, raw_table_name, unique_keys[table_name])

    if not laps_df.empty:
        load_incremental_dataframe(con, laps_df, 'raw_laps', unique_keys['raw_laps'])
    if not weather_df.empty:
        load_incremental_dataframe(con, weather_df, 'raw_weather', unique_keys['raw_weather'])
    if not race_control_df.empty:
        load_incremental_dataframe(con, race_control_df, 'raw_race_control', unique_keys['raw_race_control'])

    if not drivers_df.empty:
        load_incremental_dataframe(con, drivers_df, 'raw_drivers', unique_keys['raw_drivers'])
    if not teams_df.empty:
        load_incremental_dataframe(con, teams_df, 'raw_teams', unique_keys['raw_teams'])

    print("Ruwe data succesvol incrementieel geladen in DuckDB.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Load F1 data into DuckDB.")
    parser.add_argument('--data_directory', type=str, required=True, help="Directory containing the data files.")
    parser.add_argument('--base_url', type=str, default=None, help="Base URL for the API.")
    args = parser.parse_args()

    main(args.data_directory, args.base_url)
