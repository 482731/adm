{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

WITH base AS (
    SELECT
        circuit_name,
        location,
        country,
        latitude,
        longitude,
        altitude,
        url,
        composite_key
    FROM
        {{ ref('stg_circuits') }}
)

SELECT * FROM base
{% if is_incremental() %}
WHERE composite_key NOT IN (
    SELECT composite_key FROM {{ this }}
)
{% endif %}