WITH latest_season AS (
    SELECT MAX(year) AS latest_year
    FROM {{ref('stg_drivers_teams')}}
)

SELECT DISTINCT 
    team_name
FROM 
    {{ref('stg_drivers_teams')}}
WHERE 
    year = (SELECT latest_year FROM latest_season)
