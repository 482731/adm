{{ config(
    materialized='incremental',
    unique_key='driver_id'
) }}

WITH base AS (
    SELECT
        driver_id,
        number AS driver_number,
        code AS driver_code,
        forename AS driver_forename,
        surname AS driver_surname,
        date_of_birth,
        nationality,
        url
    FROM
        {{ ref('stg_drivers') }}
)

SELECT * FROM base
{% if is_incremental() %}
WHERE driver_id NOT IN (
    SELECT driver_id FROM {{ this }}
)
{% endif %}