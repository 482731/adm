-- Maak een dimensionele tabel met race-informatie
SELECT
    year,
    round,
    race_name,
    race_date,
    circuit_name
FROM
    {{ ref('stg_driver_results') }}
GROUP BY
    year,
    round,
    race_name,
    race_date,
    circuit_name
ORDER BY
    race_date DESC
