SELECT DISTINCT
    year
FROM
    {{ ref('stg_driver_results') }}
ORDER BY
    year DESC