{{ config(
    materialized='incremental',
    unique_key='team_id'
) }}

WITH base AS (
    SELECT
        team_id,
        team_name,
        nationality,
        url
    FROM
        {{ ref('stg_teams') }}
)

SELECT * FROM base
{% if is_incremental() %}
WHERE team_id NOT IN (
    SELECT team_id FROM {{ this }}
)
{% endif %}