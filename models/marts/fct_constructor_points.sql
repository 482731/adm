WITH constructor_points AS (
    SELECT
        year,
        round,
        race_name,
        race_date,
        team_name AS team,
        SUM(points) AS punten
    FROM
        {{ ref('stg_constructor_results') }}
    GROUP BY
        year,
        round,
        race_name,
        race_date,
        team_name
),

-- Cumulatieve puntentelling berekening
cumulative_points AS (
    SELECT
        cp.year,
        cp.round,
        cp.race_name,
        cp.race_date,
        cp.team,
        cp.punten,
        SUM(cp.punten) OVER (PARTITION BY cp.team, cp.year ORDER BY cp.race_date) AS cumulative_points
    FROM
        constructor_points cp
)

SELECT
    year,
    round,
    race_name,
    race_date,
    team,
    punten,
    cumulative_points
FROM
    cumulative_points
ORDER BY
    race_date, team