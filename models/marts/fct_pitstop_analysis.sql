-- Fact table for pitstop analysis
WITH pit_stops AS (
    SELECT
        driver_number,
        lap_number,
        pit_in_time,
        pit_out_time,
        date,
        session
    FROM
        {{ ref('stg_laps') }}
    WHERE
        (pit_in_time IS NOT NULL OR pit_out_time IS NOT NULL)
        AND session = 'Race'
),

final AS (
    SELECT
        pit_in.date,
        pit_in.session,
        pit_in.driver_number,
        pit_in.lap_number AS pit_in_lap,
        pit_in.pit_in_time,
        pit_out.lap_number AS pit_out_lap,
        pit_out.pit_out_time,
        DATEDIFF('millisecond', '00:00:00.000', pit_in.pit_in_time) AS pit_in_time_ms,
        DATEDIFF('millisecond', '00:00:00.000', pit_out.pit_out_time) AS pit_out_time_ms,
        pit_out_time_ms - pit_in_time_ms AS pit_duration_ms
    FROM
        pit_stops AS pit_in
    JOIN
        pit_stops AS pit_out
    ON
        pit_in.driver_number = pit_out.driver_number
        AND pit_in.date = pit_out.date
        AND pit_in.lap_number + 1 = pit_out.lap_number
    WHERE
        pit_in.pit_in_time IS NOT NULL
        AND pit_out.pit_out_time IS NOT NULL
)

SELECT
    driver_number,
    COUNT(*) AS number_of_pitstops,
    (CAST('00:00:00' AS TIME) + CAST(AVG(pit_duration_ms) AS BIGINT) * INTERVAL '1 millisecond') AS average_pit_time,
    date,
FROM
    final
GROUP BY
    driver_number, date
