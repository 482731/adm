-- Feitentabel voor team punten en mogelijke kampioenen analyse

WITH team_points AS (
    SELECT
        year,
        round,
        race_name,
        race_date,
        team_name AS team,
        SUM(points) AS punten
    FROM
        {{ ref('stg_constructor_results') }}
    GROUP BY
        year,
        round,
        race_name,
        race_date,
        team_name
),

-- Cumulatieve puntentelling berekening
cumulative_points AS (
    SELECT
        tp.year,
        tp.round,
        tp.race_name,
        tp.race_date,
        tp.team,
        tp.punten,
        SUM(tp.punten) OVER (PARTITION BY tp.team, tp.year ORDER BY tp.race_date) AS cumulative_points
    FROM
        team_points tp
),

-- Berekening van de huidige en maximale punten
latest_season AS (
    SELECT
        MAX(year) AS latest_year
    FROM
        {{ ref('dim_seasons') }}
),

current_standings AS (
    SELECT
        year,
        team,
        MAX(cumulative_points) AS current_points
    FROM
        cumulative_points
    GROUP BY
        year,
        team
),

max_points_remaining AS (
    -- Aangenomen dat er nog 44 punten per race te behalen zijn en 23 races in totaal
    SELECT
        year,
        44 * (23 - COUNT(DISTINCT round)) AS max_points
    FROM
        cumulative_points
    WHERE
        year = (SELECT latest_year FROM latest_season)
    GROUP BY
        year
),

possible_points AS (
    SELECT
        cs.year,
        cs.team,
        cs.current_points,
        mpr.max_points,
        cs.current_points + mpr.max_points AS possible_points
    FROM
        current_standings cs
    JOIN
        max_points_remaining mpr ON cs.year = mpr.year
),

leading_points AS (
    SELECT
        year,
        MAX(current_points) AS leading_points
    FROM
        current_standings
    GROUP BY
        year
)

-- Teams die nog kans maken op het kampioenschap
SELECT
    pp.year,
    pp.team,
    pp.current_points,
    pp.max_points,
    pp.possible_points,
    lp.leading_points
FROM
    possible_points pp
JOIN
    leading_points lp ON pp.year = lp.year
WHERE
    pp.possible_points >= lp.leading_points
ORDER BY
    pp.year, pp.team