{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        nullif(name, '') as circuit_name,  -- Renamed voor duidelijkheid
        nullif(location, '') as location,  -- Al VARCHAR, dus geen cast nodig
        nullif(country, '') as country,  -- Al VARCHAR, dus geen cast nodig
        cast(nullif(latitude, '') as double) as latitude,
        cast(nullif(longitude, '') as double) as longitude,
        cast(nullif(altitude, '') as integer) as altitude,
        nullif(url, '') as url,  -- Al VARCHAR, dus geen cast nodig
        composite_key  -- Al VARCHAR, dus geen cast nodig
    from raw_circuits
)

select * from base
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}