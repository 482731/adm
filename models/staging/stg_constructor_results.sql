{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(year, '') as integer) as year,
        cast(nullif(round, '') as integer) as round,
        nullif(name, '') as race_name,  -- Renamed voor duidelijkheid
        cast(nullif(date, '') as date) as race_date,
        nullif(team, '') as team_name,  -- Renamed voor duidelijkheid
        cast(nullif(points, '') as integer) as points,
        composite_key  -- Al VARCHAR, dus geen cast nodig
    from raw_constructor_results
)

select * from base
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}