{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(year, '') as integer) as year,
        cast(nullif(round, '') as integer) as round,
        nullif(name, '') as race_name,
        cast(nullif(date, '') as date) as race_date,
        nullif(name_1, '') as circuit_name,
        cast(nullif(carNumber, '') as integer) as car_number,
        cast(nullif(position, '') as integer) as position,
                case 
            when nullif(points, '') = '0.0' then null
            else cast(nullif(points, '') as integer)
        end as points,
        case 
            when position = 1 and time ~ '^\d{1,2}:\d{2}:\d{2}\.\d{3}$' then time
            when time ~ '^\+\d+\.\d{3}$' then replace(time, '+', '')
            else null
        end as race_time,
        nullif(reason, '') as reason,
        composite_key
    from raw_driver_results
)

select * from base
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}