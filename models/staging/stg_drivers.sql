{{ config(
    materialized='incremental',
    unique_key='driver_id'
) }}

with base as (
    select
        nullif(id, '') as driver_id,
        cast(nullif(number, '') as integer) as number,
        nullif(code, '') as code,
        nullif(forename, '') as forename,
        nullif(surname, '') as surname,
        cast(nullif(dob, '') as date) as date_of_birth,
        nullif(nationality, '') as nationality,
        nullif(url, '') as url
    from raw_drivers
)

select * from base
{% if is_incremental() %}
where driver_id not in (
    select driver_id from {{ this }}
)
{% endif %}