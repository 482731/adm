{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(year, '') as integer) as year,
        cast(nullif(race_nr, '') as integer) as race_nr,
        nullif(driver, '') as driver,
        nullif(name, '') as team_name,
        composite_key  -- Al VARCHAR, dus geen cast nodig
    from raw_drivers_teams
)

select * from base
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}