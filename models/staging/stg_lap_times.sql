{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(date, '') as date) as race_date,
        cast(nullif(carNumber, '') as integer) as car_number,
        cast(nullif(lap, '') as integer) as lap,
        cast(nullif(position, '') as integer) as position,
        case 
            when time ~ '^\d{1,2}:\d{2}\.\d{3}$' then '00:' || time
            when time ~ '^\d{1,2}:\d{2}:\d{2}\.\d{3}$' then time
            else null
        end as lap_time,
        cast(nullif(milliseconds, '') as integer) as milliseconds,
        composite_key  -- Al VARCHAR, dus geen cast nodig
    from raw_lap_times
),

converted as (
    select
        race_date,
        car_number,
        lap,
        position,
        case 
            when lap_time ~ '^\d{1,2}:\d{1,2}:\d{2}\.\d{3}$' then cast(lap_time as time)
            else null
        end as lap_time,
        milliseconds,
        composite_key
    from base
)

select * from converted
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}
