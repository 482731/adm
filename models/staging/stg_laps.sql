{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(replace("Time", '0 days ', ''), '') as time) as time,
        cast(nullif("DriverNumber", '') as integer) as driver_number,
        cast(nullif(replace("LapTime", '0 days ', ''), '') as time) as lap_time,
        cast(nullif("LapNumber", '') as integer) as lap_number,
        cast(nullif(replace("PitOutTime", '0 days ', ''), '') as time) as pit_out_time,
        cast(nullif(replace("PitInTime", '0 days ', ''), '') as time) as pit_in_time,
        cast(nullif(replace("Sector1Time", '0 days ', ''), '') as time) as sector1_time,
        cast(nullif(replace("Sector2Time", '0 days ', ''), '') as time) as sector2_time,
        cast(nullif(replace("Sector3Time", '0 days ', ''), '') as time) as sector3_time,
        cast(nullif("SpeedI1", '') as double) as speed_i1,
        cast(nullif("SpeedI2", '') as double) as speed_i2,
        cast(nullif("SpeedFL", '') as double) as speed_fl,
        cast(nullif("SpeedST", '') as double) as speed_st,
        nullif("Compound", '') as compound,
        cast(nullif("TyreLife", '') as integer) as tyre_life,
        nullif("source_file", '') as source_file,
        regexp_extract("source_file", '^(\d{8})_', 1) as date_str,
        replace(regexp_extract("source_file", '_(.*?)_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1), '_', ' ') as circuit,
        regexp_extract("source_file", '_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1) as session,
        composite_key
    from raw_laps
),


parsed_dates as (
    select
        *,
        cast(substr(date_str, 1, 4) || '-' || substr(date_str, 5, 2) || '-' || substr(date_str, 7, 2) as date) as date
    from base
)

select * from parsed_dates
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}
