{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif("timestamp", '') as timestamp) as timestamp,
        nullif("message", '') as message,
        nullif("source_file", '') as source_file,
        regexp_extract("source_file", '^(\d{8})_', 1) as date_str,
        replace(regexp_extract("source_file", '_(.*?)_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1), '_', ' ') as circuit,
        regexp_extract("source_file", '_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1) as session,
        composite_key
    from raw_race_control
),


parsed_dates as (
    select
        *,
        cast(substr(date_str, 1, 4) || '-' || substr(date_str, 5, 2) || '-' || substr(date_str, 7, 2) as date) as date
    from base
)

select * from parsed_dates
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}