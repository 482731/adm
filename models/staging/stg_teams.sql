{{ config(
    materialized='incremental',
    unique_key='team_id'
) }}

with base as (
    select
        nullif(id, '') as team_id,
        nullif(name, '') as team_name,
        nullif(nationality, '') as nationality,
        nullif(url, '') as url
    from raw_teams
)

select * from base
{% if is_incremental() %}
where team_id not in (
    select team_id from {{ this }}
)
{% endif %}
