{{ config(
    materialized='incremental',
    unique_key='composite_key'
) }}

with base as (
    select
        cast(nullif(replace(Time, '0 days ', ''), '') as time) as event_time,
        cast(nullif("AirTemp", '') as double) as air_temp,
        cast(nullif("Humidity", '') as double) as humidity,
        cast(nullif("Pressure", '') as double) as pressure,
        case when nullif("Rainfall", '') = 'true' then true else false end as rainfall,
        cast(nullif("TrackTemp", '') as double) as track_temp,
        cast(nullif("WindDirection", '') as double) as wind_direction,
        cast(nullif("WindSpeed", '') as double) as wind_speed,
        regexp_extract("source_file", '^(\d{8})_', 1) as date_str,
        replace(regexp_extract("source_file", '_(.*?)_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1), '_', ' ') as circuit,
        regexp_extract("source_file", '_(Practice|Qualifying|Race|Sprint|Sprint_Qualifying)', 1) as session,
        nullif(source_file, '') as source_file,
        nullif(composite_key, '') as composite_key
    from raw_weather
),

parsed_dates as (
    select
        *,
        cast(substr(date_str, 1, 4) || '-' || substr(date_str, 5, 2) || '-' || substr(date_str, 7, 2) as date) as date
    from base
)

select * from parsed_dates
{% if is_incremental() %}
where composite_key not in (
    select composite_key from {{ this }}
)
{% endif %}