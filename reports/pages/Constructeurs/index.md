---
title: Constructeurs Kampioenschap Analyse
---

```sql years_data
SELECT DISTINCT year FROM v_seasons ORDER BY year DESC
```

### Analyse van Constructeurs Kampioenschap

In dit rapport analyseren we de voortgang van constructeurs in het laatste seizoen en identificeren we wie nog kans maakt om het kampioenschap te winnen.


## Voortgang van het Kampioenschap

``` sql constructor_points_data
SELECT
    year,
    round,
    race_name,
    race_date,
    team,
    cumulative_points
FROM
    v_constructor_points
WHERE
    year =  ${inputs.selected_year_dropdown.value}
ORDER BY
    race_date, team
```

<Dropdown 
  data={years_data}
  name=selected_year_dropdown
  value=year
  label=year
  title="Selecteer een seizoen"
  >
</Dropdown>

<LineChart
    data={constructor_points_data}
    x=round
    xAxisTitle="Ronde"
    y=cumulative_points
    yAxisTitle="Punten"
    series=team
    title="Cumalatieve constructeurs punten"
/>

## Teams die nog een kans maken

``` sql possible_champions_data
SELECT
    team,
    current_points,
    max_points,
    possible_points,
    leading_points,
    (current_points - leading_points) as points_diff
FROM
    v_possible_champions
WHERE
    year =  ${inputs.selected_year_dropdown.value} AND possible_points >= leading_points
ORDER BY
    possible_points DESC
```

<DataTable data={possible_champions_data} rows=20>
  <Column id="team" title="Team"/>
  <Column id="current_points" title="Huidige Punten"/>
  <Column id="possible_points" title="Totale Mogelijke Punten"/>
  <Column id="points_diff" title="Puntenverschil"/>
</DataTable>