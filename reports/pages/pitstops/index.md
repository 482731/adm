---
title: Pitstop analyse
---
## Analyse van Pitstops
In dit rapport analyseren we het aantal pitstops en de gemiddelde pitstopduur per coureur tijdens de laatste race.

### Selecteer een Jaar en Race
```sql years_data
SELECT DISTINCT year FROM v_seasons ORDER BY year DESC
```
<Dropdown 
  data={years_data}
  name=selected_year_dropdown
  value=year
  label=year
  title="Selecteer een seizoen"
  >
</Dropdown>

``` sql races_data
SELECT race_name, race_date FROM v_races_by_season WHERE year = '${inputs.selected_year_dropdown.value}'
```

<Dropdown   
  data={races_data} 
  name=selected_race_dropdown
  value=race_date 
  label=race_name
  title="Selecteer een race"
  >
</Dropdown>

## Tabel van Pitstops

```sql pitstops_data
SELECT
    driver_number,
    number_of_pitstops,
    average_pit_time
FROM
    v_pitstops
WHERE
    date = ${inputs.selected_race_dropdown.value}
```

<DataTable data={pitstops_data} rows=20>
  <Column id=driver_number title="Driver Number"/>
  <Column id=number_of_pitstops title="Pitstops"/>
  <Column id=average_pit_time title="Gemiddelde pit tijd"/>
</DataTable>