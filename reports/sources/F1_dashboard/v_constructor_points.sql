SELECT
    year,
    round,
    race_name,
    race_date,
    team,
    cumulative_points
FROM
    fct_constructor_points
ORDER BY
    race_date, team

