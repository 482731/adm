-- Simple select statement to access fct_pitstop_analysis from DBT
SELECT
    driver_number,
    number_of_pitstops,
    average_pit_time,
    date
FROM
    fct_pitstop_analysis