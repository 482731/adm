-- Eenvoudige select statement om mogelijke kampioenen per jaar op te halen vanuit DBT
SELECT
    year,
    team,
    current_points,
    max_points,
    possible_points,
    leading_points
FROM
    fct_possible_champions
ORDER BY
    year, team