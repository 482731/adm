SELECT
    year,
    race_name,
    race_date
FROM
    dim_races
ORDER BY
    race_date DESC