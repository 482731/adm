# Stap 1: Start de API
cd "$env:CI_PROJECT_DIR/api"
Start-Process yarn -ArgumentList "start"
Start-Sleep -Seconds 10

# Stap 2: Start het load script
cd ..
python3 load_data.py --data_directory givenData --base_url http://localhost:3000

# Stap 3: Voer dbt uit
dbt run

# Stap 4: Voer dbt tests uit
dbt test

# Stap 5: Update evidence sources
cd "$env:CI_PROJECT_DIR/reports"
npm run sources

# Stap 6: Bouw evidence
npm run build
